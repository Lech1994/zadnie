package Zadanie2.Zadanie2;


import java.util.List;
import Db.PersonDbManager;
import Zadanie2.Zadanie2.Person;

public class App 
{
    public static void main( String[] args )
    {
        
        
        PersonDbManager mgr = new PersonDbManager();
        
        List<Person> allPersons = mgr.getAll();
        
        for(Person p : allPersons){
        	System.out.println(p.getId()+" "+p.getName()+" "+p.getSurname());
        }
        mgr.deleteById(3);
        
        Person toUpdate = new Person();
        
        toUpdate.setName("Garth Nix");
        toUpdate.setSurname("SABRIEL");
        toUpdate.setId(2);
        
        mgr.update(toUpdate);
    }
}
